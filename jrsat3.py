#! /usr/bin/python

############################################################################################################################################
############################################################################################################################################
#
# jj's unfinished Reaktor SATnet tool
#
############################################################################################################################################
############################################################################################################################################
#
#---------------------------------------------------------------------
# TODO
#---------------------------------------------------------------------
#
# - working dijkstra implementation.... :)
# - still learn more python and best practices.... 
#
# - proper cmd line argument system
# - fix: seqfault when closing mayavi window. try to catch this to properly shut down
# - fix: still change some namings
#
# Design basis of route system
# 		- build network of satellites
#		- mayavi visualizations show that often bearing to target would lead to a dead end
#		- maybe ping/traceroute flood whole net node to node until destination reached. then choose route with
#		  least hops or shortest total distance. or maybe a weighted score.
#		- recurse through all nodes, starting from all neighbors of startpoint
#		- trash all this... instead:
#
# Try to implemetn Dijkstra's algorithm for path finding
#		- Running very short on time, so this will be crude.
#		- A proper way would probably be to use SAT & GND points as just a point, and that point would be a vertex gor net graph
#		- Low time panic way: use current setup as basis for populating a net graph with vertices for Dijkstra algorithm...
#
# Class structure:
#		- problem: instead of separate sat & route, just a point with a point type -parameter would work better.
#
# (www.asciiflow.com)
#
# +-----------+         +--------------+
# | coord_xyx |         | coord_latlon |
# +------+----+         +-----+--------+
#        |                    |
#        |                    |
#   +----v--------------------v------+
#   | point(coord_xyz, coord_latlon) |
#   +----+--------------------+------+
#        |                    |
#        |                    |
# +------v-----+        +-----v--------+
# | SAT(point) |        | GND(point)   |
# +------------+        +--------------+
#
#
#
#---------------------------------------------------------------------

DESC = "jj\'s (failing) reaktor orbital challenge satellite router. Use -i CSVFILE or -g to input data. The algorithm doesn't work so use the --viz parameter to visualize output and switch to manual eyeball tracking mode to trace the path with natural eye movements... :)"

############################################################################################################################################
# imports
############################################################################################################################################

import argparse							# cmd line argument system
import urllib2							# web data fetch											TODO: fox this for python 2.7
import re								# regex
import math
import numpy as np						# n-dimentional arrays to help with vectors



############################################################################################################################################
# vars/settings
############################################################################################################################################

csvfile_existing = "data01.csv"
csv_url = "https://space-fast-track.herokuapp.com/generate"
#csv_url = "http://prism/reaktordata"	# LAN static data to avoid flooding in case of programming errors :)

# math / world
R = 6371.0								# Earth radius (km)
O = np.array([0.0, 0.0, 0.0])			# Zero reference vector
T = np.array([R+300, 0.0, 0.0])			# Test vector for LOS calculations
CT = 20.0								# LOS ground clearance warning threshold
										# LOS connections would most probably be microwave or laser, and thus too
										# easily intercepted. Theoretically a drone, balloon or jet could  be used
										# to eavesdrop or even MitM-attack low altitude comm paths! ;)

satellites = []							# list of satellite object instances
groundpoints = []						# list of ground point object instances (2 = start, end)
route = []								# final route to endpoint



############################################################################################################################################
# classes
############################################################################################################################################

# xyz coordinate for possible vector calculations
class Coord_xyz:
	x = 0.0										# coords
	y = 0.0
	z = 0.0

	def __init__(self):
		print ("xyz coord")

	# calculate XYZ coordinates, using lat, lon and height from origin
	def latlon_to_xyz(self, lat, lon, h_orig):
		# convert degrees to radians
		radLAT = float(lat) * math.pi / 180		# lat deg to rad
		radLON = float(lon) * math.pi / 180		# lon deg to rad
		
		# right-hand cartesian XYZ.
		self.x = h_orig * math.cos(radLAT) * math.cos(radLON)	# X: positve towards the prime meridian, on the equator. (is there a name for that "0" spot..?!)
		self.y = h_orig * math.cos(radLAT) * math.sin(radLON)	# Y: west (-), east (+)
		self.z = h_orig * math.sin(radLAT)						# Z: north (+), south (-)

		# create "vector" from XYZ
		self.vector = np.array([self.x, self.y, self.z])


# lat lon h coordinate
class Coord_latlon:
	lat = 0.0									# latitude (deg)
	lon = 0.0									# longitude (deg)
	h_agl = 0.0									# height (km) (Above Ground Level, AGL)
	h_orig = 0.0								# true distance from origin

	def __init__(self):
		self.h_orig = self.h_orig + R			# set true h from origin							TODO: is this actually useful or not?? decide what type of h shoild be used. True vs AGL

	# set height
	def set_h(self, attr):						# setter for modifying h
		self.h_agl = float(attr)				# AGL height
		self.h_orig = float(attr) + R			# generate true h from AGL height


# point
class Point(Coord_xyz, Coord_latlon):
	pname = "point name"
	LOS_neighbors = []
	LOS_neighbors_distance = []

	def __init__(self):
		print ("calling Point")

	# check for LOS improved
	def check_LOS_2(self, v_own, v_target, target):
		# self-to-target vector
		st = v_target - v_own

		# self-to-tager vector length (Distance to target)
		stl = np.linalg.norm(st)

		# if short distance, assume LOS
		# calculate threshold LOS distance for ground to low orbit satellite with pythagoras, solve for d:
		# (R+300)^2 + d^2 = R^2
		# d^2 = 3912600
		# d = sqrt(3912600) = 1978.029... =~ 1978
		if stl <= 1978:
			self.LOS_neighbors.append(target)
			self.LOS_neighbors_distance.append(stl)

			if args.print_loscalc == True:
				print_LOScalc(self.pname, target.pname, v_own, v_target, st, stl, "na", "na", "na")

		# if long distance, use st vector perpendicular distance to origin
		else:

			# vector cross product of self-to-target and origin-to-self
			cp = np.cross((v_target - v_own), (v_own-O))

			# length of cross product vector
			cpl = np.linalg.norm(cp)

			# shortest distance from self-to-target line to origin
			rtest = cpl / stl

			# add to LOS neighbor list if match
			if rtest > R:
				self.LOS_neighbors.append(target)
				self.LOS_neighbors_distance.append(stl)

	# check LOS for groundpoints
	def check_LOS_gnd(self, v_own, v_target, target, h_target):

		# self-to-target vector
		st = v_target - v_own

		# self-to-tager vector length (Distance to target)
		stl = np.linalg.norm(st)

		#dynamic pythagoras for ground-to-air LOS check
		d = math.sqrt((R+h_target)**2 - R**2)

		# add tos neighbor list
		if stl <= d:
			self.LOS_neighbors.append(target)
			self.LOS_neighbors_distance.append(stl)

# point.SAT
class SAT(Point):
	#LOS_neighbors = []

	def __init__(self, name, lat, lon, h):
		self.pname = str(name)
		self.lat = float(lat)
		self.lon = float(lon)
		self.set_h(h)
		self.latlon_to_xyz(lat, lon, self.h_orig)
		self.LOS_neighbors = []
		self.LOS_neighbors_distance = []


# point.GND
class GND(Point):

	def __init__(self, name, lat, lon):
		self.pname = str(name)
		self.lat = float(lat)
		self.lon = float(lon)
		#set height at surface
		self.set_h(0.0)
		self.latlon_to_xyz(lat, lon, self.h_orig)
		self.LOS_neighbors = []
		self.LOS_neighbors_distance = []

# Graph & Vertex
# Studying these sources:
# https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm#Pseudocode
# http://interactivepython.org/runestone/static/pythonds/index.html
# http://www.bogotobogo.com/python/python_Dijkstras_Shortest_Path_Algorithm.php
# http://www.gitta.info/Accessibiliti/en/html/Dijkstra_learningObject1.html

# Graph (for dijkstra)
class Graph:
	def __init__(self):
		self.vertexlist = {}							# vertices (sats & groundpoints)
		self.num_vertices = 0							# number of vertices in graph

	# what to do when iterating through stuff
	def __iter__(self):
		return iter(self.vertexlist.values())

	# add Vertex
	def add_vertex(self, node, name):
		self.num_vertices = self.num_vertices + 1		# grow list counter
		newVertex = Vertex(node, name)					# create new instance
		self.vertexlist[node] = newVertex				# add instance to vertexlist
		return newVertex								

	# get vertex
	def get_vertex(self, node):
		#if node in self.vertexlist:
		return self.vertexlist[node]					# get vertex only if it in the list
		#else:
		#	return None

	# get all vertices (to avoid calling SAT objects instead onf Vertex objects when iterating for vertices)
	def get_vertices(self):
		return self.vertexlist.keys()

	# add edge (LOS path A to B)
	def add_edge(self, a, b, cost=0):
		# check if source is not in graph vertexlist
		if a not in self.vertexlist:
			#print ("source edge not in vertexlist")
			discard = self.add_vertex(a, a.pname)
		# check if target is not in graph vertexlist
		if b not in self.vertexlist:
			#print ("destination edge not in vertexlist")
			discard = self.add_vertex(b, b.pname)
		self.vertexlist[a].add_neighbor(self.vertexlist[b], cost)

# vertice in graph
class Vertex:
	def __init__(self, node, name):
		self.id = node									# vertex id
		self.neighbors = {}								# neighbors dictinary
		self.distance = 999999999						# path distance to vertex. default to very high (infinity in proper Dijkstra)
		self.visited = False							# visited status. default to false
		self.previous = None							# previous hop
		self.vname = name								# set human readable vertex name

	# add neighbor to neighbors list (dictionary) with weight
	def add_neighbor(self, neighbor, weight=0):
		self.neighbors[neighbor] = weight

	# get neighbors of vertex
	def get_neighbors(self):
		return self.neighbors.keys()

	# get vertex id
	def get_id(self):
		return self.id

	# get weight to neighbor
	def get_weight(self, neighbor):
		return self.neighbors[neighbor]

	# set distance
	def set_distance(self, distance):
		self.distance = distance

	# get distance
	def get_distance(self):
		return self.distance

	# set previous
	def set_previous(self, prev):
		self.previous = prev

	# get previous
	def get_previous(self):
		return self.previous

	#def __str__(self):
	#	return str(self.id) + " neigh: " + str([x.id for x in self.neighbors])



############################################################################################################################################
# functions
############################################################################################################################################

# basic reading of existing csv file
def open_csv(csvfile_existing):

	print ("using " + args.csvfile)
	print ("")
	
	# error handling
	try:

		# open csvfile
		with open(csvfile_existing) as csvfile:

			# read into var
			csvFileLines = csvfile.read()

			# process data
			csv_process(csvFileLines, False)

	# if failage...
	except Exception as e:
		print ("CSV read error:")
		print(e)


# fetch fresh generated data online
def get_csv():
	print ("Using: " + csv_url)
	print ("")

	# create request, response & get response data
	req = urllib2.Request(csv_url)
	resp = urllib2.urlopen(req)
	respData = resp.read()

	# write response data to file
	saveFile = open('data02web.csv', 'w')
	saveFile.write(str(respData))
	#saveFile.write(str(resp.read()))
	saveFile.close()

	# process data
	csv_process(respData, True)


# process CSV data																
def csv_process(csvData, csvDataWeb):

	# web CSV
	if csvDataWeb == True:
		print ("csv from web")
		print ("")

	# file CSV
	else:
		print ("csv from file")
		print ("")

	# find all SAT rows
	satrows = re.findall(r'(SAT.*)', str(csvData))
	# find GND row
	routerow = re.findall(r'(ROUTE.*)', str(csvData))

	# parse satrows
	satnum = 0
	for row in satrows:
		
		sat = re.split(",", row)

		# create SAT instance																		TODO: decide if this needs a function
		satellites.append(SAT(str(sat[0].lower()), sat[1], sat[2], sat[3]))

		#increment satellites list index
		satnum += 1

	# parse route
	for i in routerow:
		route = re.split(",", i)

		# create GND point instance
		groundpoints.append(GND("start", route[1], route[2]))
		groundpoints.append(GND("end", route[3], route[4]))


# calculate LOS neighbors
def create_LOS_net():
	
	if args.print_loscalc == True:
		print ("//////////////////////////////////////////////////")
		print ("LOS calculate")
		print ("//////////////////////////////////////////////////")

	# for each satellite																			TODO: fix to just iterate through all points?	
	for SAT in satellites:
		if args.print_loscalc == True:
			print (SAT.pname)
		
		# iterate through every satellite
		for i in satellites:

			#except the one being processed
			if i != SAT:
				SAT.check_LOS_2(SAT.vector, i.vector, i)

		# iterate through endpoints
		for i in groundpoints:
			SAT.check_LOS_gnd(SAT.vector, i.vector, i, SAT.h_agl)
		
		if args.print_loscalc == True:
			print ("-- neighbors: " + str(len(SAT.LOS_neighbors)))
			print ("")

	# for each groundpoint
	for GND in groundpoints:

		#iterate through every satellite
		for i in satellites:
			GND.check_LOS_gnd(GND.vector, i.vector, i, i.h_agl)

	if args.print_loscalc == True:
		print ("//////////////////////////////////////////////////")


# create graph for dijkstra
def create_graph():
	g = Graph()

	# cycle through start and end point
	for GND in groundpoints:
		# add groundpoint as vertex to graph
		g.add_vertex(GND, GND.pname)

		# cycle through groundpoint neighbors
		i = 0
		for n in GND.LOS_neighbors:
			# add groundpoint + neighbor as route (edge in graph terms)
			g.add_edge(GND, n, n.LOS_neighbors_distance[i])
			i += 1

	# cycle through satellites
	for SAT in satellites:
		# add satellite as vertex to graph
		g.add_vertex(SAT, SAT.pname)

		# cycle through SAT neighbors
		i = 0
		for n in SAT.LOS_neighbors:
			# add SAT + neighbor as route (edge in graph terms)
			g.add_edge(SAT, n, n.LOS_neighbors_distance[i])

	return g


# dijkstra algorithm
# NOTE: very much unfinished, not properly implemented and not working...
def dijkstra(graph, start):
	target = graph.vertexlist[groundpoints[1]]
	#print ("")
	#print ("//////////////////////////////////////// dijkstra")
	#print ("start node: " + str(start.vname))
	#print ("")

	# set start distance to 0
	start.set_distance(0)

	#print (len(graph.vertexlist))
	while graph.vertexlist != []:
		
		min_distance_sofar = 999999999 + 1
		
		#cycle through vertices in graph
		for v in graph.vertexlist:
			#print graph.get_vertex(v).vname
			# if vertex distance is lower than minimum by far
			if graph.get_vertex(v).get_distance() < min_distance_sofar:
				# set vertex as current
				u = v
				min_distance_sofar = graph.get_vertex(v).get_distance()
				#print (min_distance_sofar)
		#print ("min dist:\t" + str(u.pname) + "\twith distance:\t" + str(graph.get_vertex(u).get_distance()))

		# remove u (current vertex, with minimum distance) from vertexlist
		print ("u: " + str(u.pname))
		

		if graph.vertexlist[u] == target:
			print ("")
			print ("reach end")
			s = []
			u = target
			#print (u.get_previous())
			while u.get_previous() != None:
				print ("while")
				s.append(u)
				u = u.get_previous()
			s.append(u)
			#print (s)
			#exit (0)
			return

		

		#print ("list lenght: " + str(len(graph.vertexlist)))
		#print ("")

		# for each neighbor of u (current vertex)
		for v in graph.vertexlist[u].get_neighbors():
			#print (graph.get_vertex(u).get_distance())
			#print (graph.get_vertex(u).get_weight(v))
			alt = graph.get_vertex(u).get_distance() + graph.get_vertex(u).get_weight(v)
			if alt < graph.vertexlist[u].get_distance():
				graph.vertexlist[v].set_distance(alt)
				graph.vertexlist[v].set_previous(u)

		
		del graph.vertexlist[u]

		#exit (0)


		



	print ("//////////////////////////////////////// dijkstra end")
	print ("")


# print SAT & GND info list
def print_points():
	print ("__________________________________________________")

	# satellites
	for SAT in satellites:
		print ("________________________________________")
		print (SAT.pname)
		print ("--------------------")
		print ("Lat:\t\t\t" + str(round(SAT.lat, 6)))
		print ("Lon:\t\t\t" + str(round(SAT.lon, 6)))
		print ("h AGL:\t\t\t" + str(round(SAT.h_agl, 3)))
		print ("h orig:\t\t\t" + str(round(SAT.h_orig, 3)))
		print ("----------")
		print ("X:\t\t\t" + str(round(SAT.x, 6)))
		print ("Y:\t\t\t" + str(round(SAT.y, 6)))
		print ("Z:\t\t\t" + str(round(SAT.z, 6)))
		print ("vect:\t\t\t" + str(SAT.vector))
		print ("----------")
		
		# print neighbors if any
		if SAT.LOS_neighbors:
			print ("neighbors: \t\tdistance\t\t" + "total: " + str(len(SAT.LOS_neighbors)))

			n = 0
			for i in SAT.LOS_neighbors:
					print (" " + str(i.pname) + "\t\t\t" + str(round(SAT.LOS_neighbors_distance[n], 5)))
					n += 1
		
		print ("")

	# groundpoints
	for GND in groundpoints:
		print ("________________________________________")
		print ("GROUNDPOINT")
		print ("--------------------")
		print ("Lat:\t\t\t" + str(round(GND.lat, 6)))
		print ("Lon:\t\t\t" + str(round(GND.lon, 6)))
		print ("----------")
		print ("X:\t\t\t" + str(round(GND.x, 6)))
		print ("Y:\t\t\t" + str(round(GND.y, 6)))
		print ("Z:\t\t\t" + str(round(GND.z, 6)))
		print ("vect:\t\t\t" + str(GND.vector))
		print ("----------")
		print ("________________________________________")


	print ("__________________________________________________")


# print neighbor lists
def print_neigh():
	print ("__________________________________________________")

	# satellites
	for SAT in satellites:
		print ("________________________________________")
		print (SAT.pname)

		# print neighbors if any
		if SAT.LOS_neighbors:
			n = 0
			for i in SAT.LOS_neighbors:
				print ("--\t" + str(i.pname) + "\t\t" + str(round(SAT.LOS_neighbors_distance[n], 5)))
				n += 1

	#groundpoints
	for GND in groundpoints:
		print ("________________________________________")
		print (GND.pname)

		if GND.LOS_neighbors:
			n = 0
			for i in GND.LOS_neighbors:
				print ("--\t" + str(i.pname) + "\t\t" + str(round(GND.LOS_neighbors_distance[n], 5)))
				n += 1

	print ("__________________________________________________")


# print LOS calculation data
def print_LOScalc(selfname, targetname, v_own, v_target, st, stl, cp, cpl, rtest):
	print ("//////////////////////////////////////////////////")
	print ("// try:\t\t\t" + str(selfname) + " to " + str(targetname))
	print ("// self:\t\t" + str(v_own))
	print ("// target:\t\t" + str(v_target))
	print ("// s to t:\t\t" + str(st))
	print ("// st len:\t\t" + str(stl))
	print ("// cp:\t\t\t" + str(cp))
	print ("// cp len:\t\t" + str(cpl))
	print ("// rtest:\t\t" + str(rtest))
	#if rtest > R:
	#	print ("")
	#	print ("// clearance:\t\t" + str(rtest-R))
	#	if rtest-R < CT:
	#		print ("// \t\t\tGround clearange / security warning!!")
	#	print ("// \t\t\tLOS !")
	print ("//////////////////////////////////////////////////")
	print ("")
	print ("")


# visualize with mayavi
def mayavi_3d():
	print ("==============================")
	print ("visualize")
	print ("")

	# this mayavi code is mostly done with the mayavi docs Flightgraph example:
	# http://docs.enthought.com/mayavi/mayavi/auto/example_flight_graph.html
	#
	# i'm not even quite sure what all this does... but the result should be a visual aid in checking calculations.
	# for first versions the output will be scaled to roughly match, but hopefully this can be fixed to be accurate.

	from mayavi import mlab

	# create scene (in mayavi figure = scene)
	figure = mlab.figure(1, bgcolor=(0, 0, 0), fgcolor=(0,0,0), size=(800,800))

	# clear scene
	mlab.clf()

	# coordinate arrays for sat points. don't know if this is the proper way to do this..
	xn = []
	yn = []
	zn = []
	sn = []		#sat names
	x = []
	y = []
	z = []
	xr = []
	yr = []
	zr = []

	#array for LOS lines
	LOSlines = []

	# manually figured scale for coordinate points. to match scene & sphere scale
	#sc = 6350
	sc = 6371								#figured it out! simple!!

	# label offset in km
	offset = 0
	#offset = offset/sc

	# to speed up, turn off rendering for texts in loop 
	figure.scene.disable_render = True

	# create a point to prevent text3d crash.. seems like text3d must not be first to be drawn..?
	center = mlab.points3d(0, 0, 0, scale_mode='none', scale_factor=0.01, color=(0,0,0))

	# create points for standalone satellites with no neighbors. Use manually tested scaling of /6250 to roughly match sat positions with sphere radius....
	for SAT in satellites:
		if not SAT.LOS_neighbors:
			xx = SAT.x/sc
			yy = SAT.y/sc
			zz = SAT.z/sc
			x.append(xx)
			y.append(yy)
			z.append(zz)
			# draw sat labels. text3d does'nt seem to support arrays..?!
			t = mlab.text3d(xx+offset, yy+offset, zz+offset, "  " + str(SAT.pname), scale=0.03, color=(0,0,0.6))

	# draw points for standalone sat positions
	standalone = mlab.points3d(x, y, z, scale_mode='none', scale_factor=0.03, color=(0,0,1))

	# create pointcloud for satellites in grid
	for SAT in satellites:
		if SAT.LOS_neighbors:
			xx = SAT.x/sc
			yy = SAT.y/sc
			zz = SAT.z/sc
			xn.append(xx)
			yn.append(yy)
			zn.append(zz)
			# draw sat labels.
			tt = mlab.text3d(xx+offset, yy+offset, zz+offset, "  " + str(SAT.pname), scale=0.03, color=(0,0.6,0))

	# draw points for grid sat positions
	satgrid = mlab.points3d(xn, yn, zn, scale_mode='none', scale_factor=0.03, color=(0,1,0))

	# create points for comm start- and enpoint
	for GND in groundpoints:
		xx = GND.x/sc
		yy = GND.y/sc
		zz = GND.z/sc
		xr.append(xx)
		yr.append(yy)
		zr.append(zz)
		# draw groundpoint labels.
		tt = mlab.text3d(xx+offset, yy+offset, zz+offset, "  " + str(GND.pname), scale=0.03, color=(0.6,0,0.6))

	endpoints = mlab.points3d(xr, yr, zr, scale_mode='none', scale_factor=0.03, color=(1,0,1))

	# turn rendering back on
	figure.scene.disable_render = False

	# plot LOS neighbor lines for satgrid :)
	for SAT in satellites:
		
		for i in SAT.LOS_neighbors:
			xa = SAT.x/sc
			ya = SAT.y/sc
			za = SAT.z/sc
			xb = i.x/sc
			yb = i.y/sc
			zb = i.z/sc
			#print (xa, ya, za, xb, yb, zb), uplink & downlink with different color
			if (i.pname == "end") or (i.pname == "start"):
				mlab.plot3d([xa, xb], [ya, yb], [za, zb], color=(0.5, 0, 1), tube_radius=0.003)
			else:
				mlab.plot3d([xa, xb], [ya, yb], [za, zb], color=(0, 0.5, 1), tube_radius=0.003)

	# create sphere for earth. Actually just a single point with properties
	sphere = mlab.points3d(0, 0, 0, scale_mode='none', scale_factor=2, color=(0.45, 0.55, 0.95), 
									resolution=50,
									opacity=0.8,
									name='Earth')

	# define rendering/shader properties for sphere to fix shit....
	sphere.actor.property.specular = 0.2
	sphere.actor.property.specular_power = 4
	sphere.actor.property.backface_culling = True

	# use a built in source of world continent borders to create a 3d surface
	from mayavi.sources.builtin_surface import BuiltinSurface
	continents_src = BuiltinSurface(source='earth', name='Continents')
	continents_src.data_source.on_ratio = 2
	conetinents = mlab.pipeline.surface(continents_src, color=(0, 0, 0.5,))

	# mathematically calculate equator & tropique lines. this is actually where the math gets a bit over my head...
	theta = np.linspace(0, 2 * np.pi, 100)
	for angle in (- np.pi / 6, 0, np.pi / 6):
		x = np.cos(theta) * np.cos(angle)
		y = np.sin(theta) * np.cos(angle)
		z = np.ones_like(theta) * np.sin(angle)

		mlab.plot3d(x, y, z, color=(1,1,1), opacity=0.2, tube_radius=None)

	# create view (camera properties)
	mlab.view(65, 65, 5, [0, 0, 0])

	# draw scene
	mlab.show()

	print ("==============================")


############################################################################################################################################
# command line arguments
############################################################################################################################################

#create argparser
parser = argparse.ArgumentParser(description=DESC)

#add arguments
parser.add_argument('-i', dest='csvfile', help='input CSV file from disk')
parser.add_argument('-g', dest='csvwww', action='store_true', help='fetch CSV online')
parser.add_argument('-l', dest='print_sats', action='store_true', help='print point info list')
parser.add_argument('-n', dest='print_neigh', action='store_true', help='print point neighbor list')
parser.add_argument('--loscalc', dest='print_loscalc', action='store_true', help='print calculation details')
parser.add_argument('--viz', dest='print_3d', action='store_true', help='mayavi visualization')
parser.add_argument('-t', dest='testing', action='store_true', help='test stuff :)')

#execute
args = parser.parse_args()



############################################################################################################################################
# functionality
############################################################################################################################################

# get CSV data drom file
if args.csvfile:
	open_csv(args.csvfile)

# or from the web
elif args.csvwww == True:
	get_csv()

# show help if no data input
else:
	parser.print_help()

# actual functionality
if satellites:
	# create network by calculating LOS data
	create_LOS_net()

	# generate graph with nodes as vertices
	g = create_graph()

	#print ("")
	#print ("--- sanity checks ---")
	# check we have 20 sats + 2 endpoints = 22
	#print (str(len(g.vertexlist)))
	# should print vertex id of groundpoint[0] which is startpoint
	#print (str(g.vertexlist[groundpoints[0]]))
	## should print human readable name of vertex (derived from SAT or groundpoint pname)
	#print (g.vertexlist[groundpoints[0]])
	#print ("--- sanity checks end ---")
	#print ("")
	#print ("")


	#failure
	print ("NOTE: the output below is junk. this code does not work. use the --viz parameter to see output visually. then use eyeballs to trace the path... :D")
	print ("")
	print ("")


	# calculate paths in graph
	dijkstra(g, g.get_vertex(groundpoints[0]))


	# test printing vertex neighbors in human readable names
	#for i in g.vertexlist[groundpoints[0]].get_neighbors():
	#	print i.vname
	#print ("")

	#print ("#######")
	#for i in g.get_vertices():
	#	print (g.get_vertex(i).vname)
	#print ("#######")

	# set target to find route to
	target = g.get_vertex(groundpoints[1])
	#print (target.vname)
	route = [target.get_id()]



	#print info list
	if args.print_sats == True:
		print_points()

	if args.print_neigh == True:
		print_neigh()

	if args.print_3d == True:
		mayavi_3d()

else:
	print ("missing data...")



