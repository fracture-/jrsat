# JRSAT #

jrsat, the failing path finder tool for Reaktor Orbital challenge.

### How does it work? ###

* It doesn't.
* It draws a nice Mayavi mlab 3D view with plotted LOS lines between satellites.

### How should it work? ###

* By using Dijkstra's algorithm to calculate a satnet
* Find shortest path from start groundpoint to end groundpoint

### How to use it? ###

* input data with -i or -g
* -i datafile.csv (input data in a file)
* -g (fetch data from the web)
* --viz (draw calculated satellite network with mayavi mlab)