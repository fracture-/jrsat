#! /usr/bin/python3

import math
import numpy as np
#import vectors


#vector calculations

# create XYZ vectors
a = np.array([3, 5, 2])
b = np.array([2,5,4])
c = np.array([1,3,5])

# calculate vector length
al = np.linalg.norm(a)
al2 = math.sqrt(np.dot(a,a))				#should be equal to sqrt of vector dot product by itself!

# normalize vector (divide by own lenght)
an = a/np.linalg.norm(a)

# normalized vector lenght should be 1
test = np.linalg.norm(an)


print (a)
print (al)
print (al2)
print (an)
print (test)

print ()
print ()



# test calculations in 2D

# SAT XYZ points as vectors
a = np.array([1, 4, 0])				# SAT1
b = np.array([7, -4, 0])				# SAT2
orig = np.array([0,0, 0])				# Origin

# vector from SAT to SAT
b_to_a = b-a

# distance between SATs.
distance = np.linalg.norm(b_to_a)

# dot product and cross product 
a_dot_b = np.dot(a,b)
a_cross_b = np.cross(a,b)

# shortest distance of line from SAT1 to SAT 2, to origin
#
# cross product (vector) of SAT1 & SAT2 vectors
cross = np.cross((b-a),(a-orig))
# SAT1 & SAT2 cross product vector length
cross_length = np.linalg.norm(cross)
# SAT1 to SAT2 vector
ba = b-a
# SAT1 to SAT2 cevtor length
ba_length = np.linalg.norm(ba)

# shortest distance from origin to LOS-line between SAT1 & SAT2
rtest2 = cross_length / ba_length			# Works!!! \o/

# compare LOS-line to origin distance with earth radius to test if LOS avaliable!
# 
#





print ("XYZ as vectors")
print (a)
print (b)
print (orig)
print ()
print ("vector b-a")
print (b_to_a)
print ()
print ("distace from SAT1 to SAT2")
print (distance)
print ()
print ("a*b & a x b")
print (a_dot_b)
print (a_cross_b)

print ()

print (rtest)
print (cross)
print (cross_length)
print (ba)
print (ba_length)
print (rtest2)