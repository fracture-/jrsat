#! /usr/bin/python

############################################################################################################################################
############################################################################################################################################
#
# jj's reaktor SATnet tool
#
############################################################################################################################################
############################################################################################################################################
#
#---------------------------------------------------------------------
# TODO
#---------------------------------------------------------------------
#
# - still learn more python and best practices.... 
#
# - proper cmd line argument system
# - fix: seqfault when closing mayavi window. try to catch this to properly shut down
# - fix: naming conventions. needs less confusing naming of routes, routepoints, points, etc...
#
# Design basis of route system
# 		- maybe a network with each node knowing neighbor hop bearing & distance metric..?
#		- mayavi visualizations show that often bearing to target would lead to a dead end
#		- maybe ping/traceroute flood whole net node to node until destination reached. then choose route with
#		  least hops or shortest total distance. or maybe a weighted score.
#		- recurse through all nodes, starting from all neighbors of startpoint
#
# Route/hop metric calculation
#		- metric score & decision system
#
# Class structure:
#		- problem: instead of separate sat & route, just a point with a point type -parameter would work better.
#
# (www.asciiflow.com)
#
# +-----------+         +--------------+
# | coord_xyx |         | coord_latlon |
# +------+----+         +-----+--------+
#        |                    |
#        |                    |
#   +----v--------------------v------+
#   | point(coord_xyz, coord_latlon) |
#   +----+--------------------+------+
#        |                    |
#        |                    |
# +------v-----+        +-----v--------+
# | SAT(point) |        | ROUTE(point) |
# +------------+        +--------------+
#
#
#
#---------------------------------------------------------------------

############################################################################################################################################
# imports
############################################################################################################################################

import argparse							# cmd line argument system
import urllib2							# web data fetch											TODO: fox this for python 2.7
import re								# regex
import math
import numpy as np						# n-dimentional arrays to help with vectors



############################################################################################################################################
# vars/settings
############################################################################################################################################

csvfile_existing = "data01.csv"
csv_url = "https://space-fast-track.herokuapp.com/generate"
#csv_url = "http://prism/reaktordata"	# LAN static data to avoid flooding in case of programming errors :)

# math / world
R = 6371.0								# Earth radius (km)
O = np.array([0.0, 0.0, 0.0])			# Zero reference vector
T = np.array([R+300, 0.0, 0.0])			# Test vector for LOS calculations
CT = 20.0								# LOS ground clearance warning threshold
										# LOS connections would most probably be microwave or laser, and thus too
										# easily intercepted. Theoretically a drone, balloon or jet could  be used
										# to eavesdrop or even MitM-attack low altitude comm paths! ;)

satellites = []							# list of satellite object instances
routepoints = []						# list of ground point object instances (2 = start, end)
paths = []								# list of calculated paths
route = []								# final route to endpoint



############################################################################################################################################
# classes
############################################################################################################################################

# xyz coordinate for possible vector calculations
class Coord_xyz:
	x = 0.0										# coords
	y = 0.0
	z = 0.0

	def __init__(self):
		print ("xyz coord")

	# calculate XYZ coordinates, using lat, lon and height from origin
	def latlon_to_xyz(self, lat, lon, h_orig):
		# convert degrees to radians
		radLAT = float(lat) * math.pi / 180		# lat deg to rad
		radLON = float(lon) * math.pi / 180		# lon deg to rad
		
		# right-hand cartesian XYZ.
		
		
		
		self.x = h_orig * math.cos(radLAT) * math.cos(radLON)	# X: positve towards the prime meridian, on the equator. (is there a name for that "0" spot..?!)
		self.y = h_orig * math.cos(radLAT) * math.sin(radLON)	# Y: west (-), east (+)
		self.z = h_orig * math.sin(radLAT)						# Z: north (+), south (-)

		# create "vector" from XYZ
		self.vector = np.array([self.x, self.y, self.z])


# lat lon h coordinate
class Coord_latlon:
	lat = 0.0									# latitude (deg)
	lon = 0.0									# longitude (deg)
	h_agl = 0.0									# height (km) (Above Ground Level, AGL)
	h_orig = 0.0								# true distance from origin

	def __init__(self):
		print ("calling Coord_latlon")			# see when this thing gets called
		self.h_orig = self.h_orig + R			# set true h from origin							TODO: is this actually useful or not?? decide what type of h shoild be used. True vs AGL

	# set height
	def set_h(self, attr):						# setter for modifying h
		self.h_agl = float(attr)				# AGL height
		self.h_orig = float(attr) + R			# generate true h from AGL h


# point
class Point(Coord_xyz, Coord_latlon):
	pname = "point name"
	LOS_neighbors = []
	LOS_neighbors_distance = []

	def __init__(self):
		print ("calling Point")

	# check for LOS																					OBSOLETE
	def check_LOS(self, v_own, v_target, target):
		
		# self-to-target vector
		st = v_target - v_own

		# self-to-tager vector length (Distance to target)
		stl = np.linalg.norm(st)

		# vector cross product of self-to-target and origin-to-self
		cp = np.cross((v_target - v_own), (v_own-O))

		# length of cross product vector
		cpl = np.linalg.norm(cp)

		# shortest distance from self-to-target line to origin
		rtest = cpl / stl

		# add to LOS neighbor list if match
		if rtest > R:
			self.LOS_neighbors.append(target)
			self.LOS_neighbors_distance.append(stl)
			#print ("------------------------------------------------------------------------PING")
			if args.print_loscalc == True:
				print ("-- LOS " + str(getattr(target, "pname")) + " for " + self.pname)

		# print calculation results
		if args.print_loscalc == True:
			print ("//////////////////////////////////////////////////")
			print ("// try:\t\t\t" + str(self.pname) + " to " + str(target.pname))
			print ("// self:\t\t" + str(v_own))
			print ("// target:\t\t" + str(v_target))
			print ("// s to t:\t\t" + str(st))
			print ("// st len:\t\t" + str(stl))
			print ("// cp:\t\t\t" + str(cp))
			print ("// cp len:\t\t" + str(cpl))
			print ("// rtest:\t\t" + str(rtest))
			if rtest > R:
				print ("")
				print ("// clearance:\t\t" + str(rtest-R))
				if rtest-R < CT:
					print ("// \t\t\tGround clearange / security warning!!")
				print ("// \t\t\tLOS !")
			print ("//////////////////////////////////////////////////")
			print ("")
			print ("")


	# check for LOS improved
	def check_LOS_2(self, v_own, v_target, target):
		# self-to-target vector
		st = v_target - v_own

		# self-to-tager vector length (Distance to target)
		stl = np.linalg.norm(st)

		# if short distance, assume LOS
		# calculate threshold LOS distance for ground to low orbit satellite with pythagoras, solve for d:
		# (R+300)^2 + d^2 = R^2
		# d^2 = 3912600
		# d = sqrt(3912600) = 1978.029... =~ 1978
		if stl <= 1978:
			self.LOS_neighbors.append(target)
			self.LOS_neighbors_distance.append(stl)

			if args.print_loscalc == True:
				print_LOScalc(self.pname, target.pname, v_own, v_target, st, stl, "na", "na", "na")

		# if long distance, use st vector perpendicular distance to origin
		else:

			# vector cross product of self-to-target and origin-to-self
			cp = np.cross((v_target - v_own), (v_own-O))

			# length of cross product vector
			cpl = np.linalg.norm(cp)

			# shortest distance from self-to-target line to origin
			rtest = cpl / stl

			# add to LOS neighbor list if match
			if rtest > R:
				self.LOS_neighbors.append(target)
				self.LOS_neighbors_distance.append(stl)

	# check LOS for groundpoints
	def check_LOS_gnd(self, v_own, v_target, target, h_target):

		# self-to-target vector
		st = v_target - v_own

		# self-to-tager vector length (Distance to target)
		stl = np.linalg.norm(st)

		#dynamic pythagoras for ground-to-air LOS check
		d = math.sqrt((R+h_target)**2 - R**2)

		# add tos neighbor list
		if stl <= d:
			self.LOS_neighbors.append(target)
			self.LOS_neighbors_distance.append(stl)

# point.SAT
class SAT(Point):
	#LOS_neighbors = []

	def __init__(self, name, lat, lon, h):
		self.pname = str(name)
		self.lat = float(lat)
		self.lon = float(lon)
		self.set_h(h)
		self.latlon_to_xyz(lat, lon, self.h_orig)
		self.LOS_neighbors = []
		self.LOS_neighbors_distance = []


# point.ROUTE
class ROUTE(Point):

	def __init__(self, name, lat, lon):
		self.pname = str(name)
		self.lat = float(lat)
		self.lon = float(lon)
		#set height at surface
		self.set_h(0.0)
		self.latlon_to_xyz(lat, lon, self.h_orig)
		self.LOS_neighbors = []
		self.LOS_neighbors_distance = []

# 



############################################################################################################################################
# functions
############################################################################################################################################

# basic reading of existing csv file
def open_csv(csvfile_existing):

	print ("using " + args.csvfile)
	print ("")
	
	# error handling
	try:

		# open csvfile
		with open(csvfile_existing) as csvfile:

			# read into var
			csvFileLines = csvfile.read()

			# process data
			csv_process(csvFileLines, False)

	# if failage...
	except Exception as e:
		print ("CSV read error:")
		print(e)


# fetch fresh generated data online
def get_csv():
	print ("Using: " + csv_url)
	print ("")

	# create request, response & get response data
	req = urllib2.Request(csv_url)
	resp = urllib2.urlopen(req)
	respData = resp.read()

	# write response data to file
	saveFile = open('data02web.csv', 'w')
	saveFile.write(str(respData))
	#saveFile.write(str(resp.read()))
	saveFile.close()

	# process data
	csv_process(respData, True)


# process CSV data																
def csv_process(csvData, csvDataWeb):

	# web CSV
	if csvDataWeb == True:
		print ("csv from web")
		print ("")
	#
	#	# find all SAT rows
	#	satrows = re.findall(r'(SAT.*?)(?=\\)', str(csvData))
	#	#find ROUTE row
	#	routerow = re.findall(r'(ROUTE.*?)(?=\\)', str(csvData))

	# file CSV
	else:
		print ("csv from file")
		print ("")
	#
	#	# find all SAT rows
	#	satrows = re.findall(r'(SAT.*)', str(csvData))
	#	# find ROUTE row
	#	routerow = re.findall(r'(ROUTE.*)', str(csvData))

	# seems like urllib2 gives proper output so only one regex needed
	# find all SAT rows
	satrows = re.findall(r'(SAT.*)', str(csvData))
	# find ROUTE row
	routerow = re.findall(r'(ROUTE.*)', str(csvData))

	# parse satrows
	satnum = 0
	for row in satrows:
		
		sat = re.split(",", row)

		# create SAT instance																		TODO: decide if this needs a function
		satellites.append(SAT(str(sat[0].lower()), sat[1], sat[2], sat[3]))

		#increment satellites list index
		satnum += 1

	# parse route
	for i in routerow:
		route = re.split(",", i)

		# create ROUTE point instance
		routepoints.append(ROUTE("start", route[1], route[2]))
		routepoints.append(ROUTE("end", route[3], route[4]))


# calculate LOS neighbors
def create_LOS_net():
	
	if args.print_loscalc == True:
		print ("//////////////////////////////////////////////////")
		print ("LOS calculate")
		print ("//////////////////////////////////////////////////")

	# for each satellite																			TODO: fix to just iterate through all points?	
	for SAT in satellites:
		if args.print_loscalc == True:
			print (SAT.pname)
		
		# iterate through every satellite
		for i in satellites:

			#except the one being processed
			if i != SAT:
				SAT.check_LOS_2(SAT.vector, i.vector, i)

		# iterate through endpoints
		for i in routepoints:
			SAT.check_LOS_gnd(SAT.vector, i.vector, i, SAT.h_agl)
		
		if args.print_loscalc == True:
			print ("-- neighbors: " + str(len(SAT.LOS_neighbors)))
			print ("")

	# for each groundpoint
	for ROUTE in routepoints:

		#iterate through every satellite
		for i in satellites:
			ROUTE.check_LOS_gnd(ROUTE.vector, i.vector, i, i.h_agl)

	if args.print_loscalc == True:
		print ("//////////////////////////////////////////////////")


# recursively flood & trace network for possible paths to endpoint. unefficcient but should someday get the job done.
def find_paths(ssat, depth, hops):
	print ("==================== find paths")
	
	# path sat list
	path_attempt = []
	#hops = 0
	# recurse neighbors
	def recurse_neigh(current, previous, depth, hops):
		print ("---------- recurse:\t" + str(previous.pname) + " ---> " + str(current.pname) + "\t\t with: " + str(depth))
		
		# add current hop to path
		path_attempt.append(previous)

		# check for depth limit
		if depth != 0:

			# set trigger
			go = False
			print ("depth non zero: " + str(depth))

			# continue for each current hop neighbor
			for n in current.LOS_neighbors:
				
				print ("")
				print ("try: " + str(n.pname))
				
				# check for path existing hops
				for x in path_attempt:
			
					print ("fail: " + str(x.pname))
			
					# not a possible neighbor
					if x == n:
						# no good, skip
						print ("visited node, break")
						print ("")
						go = False
						continue												# break vs continue?
			
					# reached target
					elif n.pname == "end":
						print ("ENDPOINT")
			#			for hop in path_attempt:
			#				print (hop.pname)
			#			
			#			#clear path for next attempt
						del path_attempt[:]
						go = False
						#continue
			#
					else:
						print ("--->\t\t\t" + str(n.pname))
						#print ("--->goto\t\t" + str(n.pname) + "\tfrom\t" + str(current.pname) + " with\t" + str(depth-1))
						#recurse_neigh(n, current, depth-1)
						#break
						go = True
					#	print ("go = true")
						#continue
			#
			#	# no trigger
				if go == False:
					print ("gofalse, break")
					print ("")
			#		break
			#
				# trigger set
				elif go == True:
					print ("go = true")
					print ("--->goto\t\t" + str(n.pname) + "\tfrom\t" + str(current.pname) + " with\t" + str(depth-1))
					go = False
					hops += 1
					recurse_neigh(n, current, depth-1, hops)
			#		break
			#
			#return out of recurse_neigh after go = false
			#return
						
		#hop limit reached
		else:
			print ("depth zero: " + str(depth))
			print ("")
			return
			#return finish()

		print ("---------- end recurse")
		print ("")

	print ("1st recurse:\t" + str(ssat.pname) + " depth: " + str(depth))
	print ("")
	recurse_neigh(ssat, routepoints[0], depth-1, hops)

	print ("==================== find paths end")
	print ("")
	#return



# decide final route
def find_route():
	#generate paths starting with LOS first hops
	for uplink in routepoints[0].LOS_neighbors:
		print ("------------------------------------------------------------------------------uplink " + str(uplink.pname))
		find_paths(uplink, 3, 0)

# print SAT info																					OBSOLETE
def satinfo_print(sat):
	print ("__________")
	print (getattr(satellites[sat], "pname"))
	print ("----------")
	print ("Lat:\t\t" + str(getattr(satellites[sat], "lat")))
	print ("Lon:\t\t" + str(getattr(satellites[sat], "lon")))
	print ("h AGL:\t\t" + str(getattr(satellites[sat], "h_agl")))
	print ("R+h:\t\t" + str(getattr(satellites[sat], "h_orig")))
	print ("----------")
	print ("X:\t\t" + str(round(getattr(satellites[sat], "x"), 6)))
	print ("Y:\t\t" + str(round(getattr(satellites[sat], "y"), 6)))
	print ("Z:\t\t" + str(round(getattr(satellites[sat], "z"), 6)))
	print ("----------")
	print ("Vector:\t\t" + str(getattr(satellites[sat], "vector")))
	print ("----------")
	if getattr(satellites[sat], "LOS_neighbors"):
		print ("neighbors")
	else:
		print ("no neighbour info / LOS data")
	print ("__________")
	print ("")


# print ROUTE info																					OBSOLETE
def routeinfo_print(route):
	print ("")
	print ("==========")
	print ("ROUTE")
	print ("----------")
	print ("A Lat:\t\t" + str(getattr(routepoints[0], "lat")))
	print ("A Lat:\t\t" + str(getattr(routepoints[0], "lon")))
	print ("")
	print ("X:\t\t" + str(round(getattr(routepoints[0], "x"), 6)))
	print ("Y:\t\t" + str(round(getattr(routepoints[0], "y"), 6)))
	print ("Z:\t\t" + str(round(getattr(routepoints[0], "z"), 6)))
	print ("")
	print ("Vector:\t\t" + str(getattr(routepoints[0], "vector")))
	print ("----------")
	print ("B Lat:\t\t" + str(getattr(routepoints[1], "lat")))
	print ("B Lat:\t\t" + str(getattr(routepoints[1], "lon")))
	print ("")
	print ("X:\t\t" + str(round(getattr(routepoints[1], "x"), 6)))
	print ("Y:\t\t" + str(round(getattr(routepoints[1], "y"), 6)))
	print ("Z:\t\t" + str(round(getattr(routepoints[1], "z"), 6)))
	print ("")
	print ("Vector:\t\t" + str(getattr(routepoints[0], "vector")))
	print ("==========")

	print ("")


# print SAT & ROUTE info list
def print_points():
	print ("__________________________________________________")

	# satellites
	for SAT in satellites:
		print ("________________________________________")
		print (SAT.pname)
		print ("--------------------")
		print ("Lat:\t\t\t" + str(round(SAT.lat, 6)))
		print ("Lon:\t\t\t" + str(round(SAT.lon, 6)))
		print ("h AGL:\t\t\t" + str(round(SAT.h_agl, 3)))
		print ("h orig:\t\t\t" + str(round(SAT.h_orig, 3)))
		print ("----------")
		print ("X:\t\t\t" + str(round(SAT.x, 6)))
		print ("Y:\t\t\t" + str(round(SAT.y, 6)))
		print ("Z:\t\t\t" + str(round(SAT.z, 6)))
		print ("vect:\t\t\t" + str(SAT.vector))
		print ("----------")
		
		# print neighbors if any
		if SAT.LOS_neighbors:
			print ("neighbors: \t\tdistance\t\t" + "total: " + str(len(SAT.LOS_neighbors)))

			n = 0
			for i in SAT.LOS_neighbors:
					print (" " + str(i.pname) + "\t\t\t" + str(round(SAT.LOS_neighbors_distance[n], 5)))
					n += 1
		
		print ("")

	# groundpoints
	for ROUTE in routepoints:
		print ("________________________________________")
		print ("GROUNDPOINT")
		print ("--------------------")
		print ("Lat:\t\t\t" + str(round(ROUTE.lat, 6)))
		print ("Lon:\t\t\t" + str(round(ROUTE.lon, 6)))
		print ("----------")
		print ("X:\t\t\t" + str(round(ROUTE.x, 6)))
		print ("Y:\t\t\t" + str(round(ROUTE.y, 6)))
		print ("Z:\t\t\t" + str(round(ROUTE.z, 6)))
		print ("vect:\t\t\t" + str(ROUTE.vector))
		print ("----------")
		print ("________________________________________")


	print ("__________________________________________________")


# print neighbor lists
def print_neigh():
	print ("__________________________________________________")

	# satellites
	for SAT in satellites:
		print ("________________________________________")
		print (SAT.pname)

		# print neighbors if any
		if SAT.LOS_neighbors:
			n = 0
			for i in SAT.LOS_neighbors:
				print ("--\t" + str(i.pname) + "\t\t" + str(round(SAT.LOS_neighbors_distance[n], 5)))
				n += 1

	#groundpoints
	for ROUTE in routepoints:
		print ("________________________________________")
		print (ROUTE.pname)

		if ROUTE.LOS_neighbors:
			n = 0
			for i in ROUTE.LOS_neighbors:
				print ("--\t" + str(i.pname) + "\t\t" + str(round(ROUTE.LOS_neighbors_distance[n], 5)))
				n += 1

	print ("__________________________________________________")


# print LOS calculation data
def print_LOScalc(selfname, targetname, v_own, v_target, st, stl, cp, cpl, rtest):
	print ("//////////////////////////////////////////////////")
	print ("// try:\t\t\t" + str(selfname) + " to " + str(targetname))
	print ("// self:\t\t" + str(v_own))
	print ("// target:\t\t" + str(v_target))
	print ("// s to t:\t\t" + str(st))
	print ("// st len:\t\t" + str(stl))
	print ("// cp:\t\t\t" + str(cp))
	print ("// cp len:\t\t" + str(cpl))
	print ("// rtest:\t\t" + str(rtest))
	#if rtest > R:
	#	print ("")
	#	print ("// clearance:\t\t" + str(rtest-R))
	#	if rtest-R < CT:
	#		print ("// \t\t\tGround clearange / security warning!!")
	#	print ("// \t\t\tLOS !")
	print ("//////////////////////////////////////////////////")
	print ("")
	print ("")


# visualize with mayavi
def mayavi_3d():
	print ("==============================")
	print ("visualize")
	print ("")
	print ("")

	# this mayavi code is mostly done with the mayavi docs Flightgraph example:
	# http://docs.enthought.com/mayavi/mayavi/auto/example_flight_graph.html
	#
	# i'm not even quite sure what all this does... but the result should be a visual aid in checking calculations.
	# for first versions the output will be scaled to roughly match, but hopefully this can be fixed to be accurate.

	from mayavi import mlab

	# create scene (in mayavi figure = scene)
	mlab.figure(1, bgcolor=(0, 0, 0), fgcolor=(0,0,0), size=(800,800))

	# clear scene
	mlab.clf()

	# coordinate arrays for sat points. don't know if this is the proper way to do this..
	xn = []
	yn = []
	zn = []
	x = []
	y = []
	z = []
	xr = []
	yr = []
	zr = []

	#array for LOS lines
	LOSlines = []

	# manually figured scale for coordinate points. to match scene & sphere scale
	#sc = 6350
	sc = 6371								#figured it out! simple!!

	# create points for standalone satellites with no neighbors. Use manually tested scaling of /6250 to roughly match sat positions with sphere radius....
	for SAT in satellites:
		if not SAT.LOS_neighbors:
			x.append(SAT.x/sc)
			y.append(SAT.y/sc)
			z.append(SAT.z/sc)

	# create points for sat positions
	standalone = mlab.points3d(x, y, z, scale_mode='none', scale_factor=0.03, color=(0,0,1))

	# create pointcloud for satellites in grid
	for SAT in satellites:
		if SAT.LOS_neighbors:
			xn.append(SAT.x/sc)
			yn.append(SAT.y/sc)
			zn.append(SAT.z/sc)
	satgrid = mlab.points3d(xn, yn, zn, scale_mode='none', scale_factor=0.03, color=(0,1,0))

	# create points for comm start- and enpoint
	for ROUTE in routepoints:
		xr.append(ROUTE.x/sc)
		yr.append(ROUTE.y/sc)
		zr.append(ROUTE.z/sc)
	endpoints = mlab.points3d(xr, yr, zr, scale_mode='none', scale_factor=0.03, color=(1,0,1))

	# plot LOS neighbor lines for satgrid :)
	for SAT in satellites:
		
		for i in SAT.LOS_neighbors:
			xa = SAT.x/sc
			ya = SAT.y/sc
			za = SAT.z/sc
			xb = i.x/sc
			yb = i.y/sc
			zb = i.z/sc
			#print (xa, ya, za, xb, yb, zb), uplink & downlink with different color
			if (i.pname == "end") or (i.pname == "start"):
				mlab.plot3d([xa, xb], [ya, yb], [za, zb], color=(0.5, 0, 1), tube_radius=0.003)
			else:
				mlab.plot3d([xa, xb], [ya, yb], [za, zb], color=(0, 0.5, 1), tube_radius=0.003)

	# create sphere for earth. Actually just a single point with properties
	sphere = mlab.points3d(0, 0, 0, scale_mode='none', scale_factor=2, color=(0.65, 0.75, 0.95), 
									resolution=50,
									opacity=0.7,
									name='Earth')

	# define rendering/shader properties for sphere to fix shit....
	sphere.actor.property.specular = 0.2
	sphere.actor.property.specular_power = 4
	sphere.actor.property.backface_culling = True

	# use a built in source of world continent borders to create a 3d surface
	from mayavi.sources.builtin_surface import BuiltinSurface
	continents_src = BuiltinSurface(source='earth', name='Continents')
	continents_src.data_source.on_ratio = 2
	conetinents = mlab.pipeline.surface(continents_src, color=(0, 0, 0.5,))

	# mathematically calculate equator & tropique lines. this is actually where the math gets a bit over my head...
	theta = np.linspace(0, 2 * np.pi, 100)
	for angle in (- np.pi / 6, 0, np.pi / 6):
		x = np.cos(theta) * np.cos(angle)
		y = np.sin(theta) * np.cos(angle)
		z = np.ones_like(theta) * np.sin(angle)

		mlab.plot3d(x, y, z, color=(1,1,1), opacity=0.2, tube_radius=None)

	# create view (camera properties)
	mlab.view(65, 65, 5, [0, 0, 0])

	# draw scene
	mlab.show()

	print ("==============================")


#exit
def finish():
	print ("finish")


############################################################################################################################################
# command line arguments
############################################################################################################################################

#create argparser
parser = argparse.ArgumentParser(description='jj\'s reaktor orbital challenge satellite router')

#add arguments
parser.add_argument('-i', dest='csvfile', help='input CSV file from disk')
parser.add_argument('-g', dest='csvwww', action='store_true', help='fetch CSV online')
parser.add_argument('-l', dest='print_sats', action='store_true', help='print point info list')
parser.add_argument('-n', dest='print_neigh', action='store_true', help='print point neighbor list')
parser.add_argument('--loscalc', dest='print_loscalc', action='store_true', help='print calculation details')
parser.add_argument('--viz', dest='print_3d', action='store_true', help='mayavi visualization')
parser.add_argument('-t', dest='testing', action='store_true', help='test stuff :)')

#execute
args = parser.parse_args()



############################################################################################################################################
# functionality
############################################################################################################################################

# get CSV data drom file
if args.csvfile:
	open_csv(args.csvfile)

# or from the web
elif args.csvwww == True:
	get_csv()

# show help if no data input
else:
	parser.print_help()

# actual functionality
if satellites:
	# create network by calculating LOS data
	create_LOS_net()

	# calculate paths
	find_route()

	#print info list
	if args.print_sats == True:
		print_points()

	if args.print_neigh == True:
		print_neigh()

	if args.print_3d == True:
		mayavi_3d()

else:
	print ("missing data...")



